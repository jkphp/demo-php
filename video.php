<?php
 
  $xml = simplexml_load_file('https://www.youtube.com/watch?v=o0fE0ufIQnQ&index=8&list=PLhWZuBds8ldbJ2CXeQ8WsUsTNEhDwmQ4U');
//echo '<pre>'; print_r($xml);die;
  $server_time = $xml->updated;
 
  $return = array();
 
  foreach ($xml->entry as $video) {
      $vid = array();
 
      $vid['id'] = substr($video->id,42);
      $vid['title'] = $video->title;
      $vid['date'] = $video->published;
      //$vid['desc'] = $video->content;
 
      // get nodes in media: namespace for media information
      $media = $video->children('http://search.yahoo.com/mrss/');
 
      // get the video length
      $yt = $media->children('http://gdata.youtube.com/schemas/2007');
      $attrs = $yt->duration->attributes();
      $vid['length'] = $attrs['seconds'];
 
      // get video thumbnail
      $attrs = $media->group->thumbnail[0]->attributes();
      $vid['thumb'] = $attrs['url'];
 
      // get <yt:stats> node for viewer statistics
      $yt = $video->children('http://gdata.youtube.com/schemas/2007');
      $attrs = $yt->statistics->attributes();
      $vid['views'] = $attrs['viewCount'];
 
      array_push($return, $vid);
  }
 
  ?>
 
<style>
body
{
  font-size:12px;
  font-family:tahoma;
}
h2
{
  font-size:13px;
}
p
{
  line-height:5px;
}
.youtube-video-gallery
{
  width:930px;
  margin:auto;
}
.clearfix
{
  clear:both;
}
.youtube-video
{
  padding:10px;
  background-color:#d8d8d8;
  width:270px;
  float:left;
  margin:10px;
}
</style>
<div class="youtube-video-gallery">
  <?php
  foreach($return as $video) {
 
    ?>
      <div class="youtube-video">
        <a href="https://www.youtube.com/watch?v=<?=$video['id']?>" target="_blank">
          <img src="<?= $video['thumb']?>" width="270"/>
        </a>
        <h2>
          <a href="https://www.youtube.com/watch?v=<?=$video['id']?>" target="_blank">
            <?= $video['title'] ?>
          </a>
        </h2>
        <p>Views: <?=$video['views']?></p>
        <p>Length: <?=$video['length']?></p>
        <p>Published on: <?=$video['date']?></p>
 
      </div>
    <?php
  }
?>
<div class="clearfix"></div>
</div>